package firstday.tailt.pixastudio.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import firstday.tailt.pixastudio.Model.ChildMovie;
import firstday.tailt.pixastudio.demomoviedatabase.R;

/**
 * Created by tai on 09/10/2016.
 */

public class MainActivityAdapter extends RecyclerView.Adapter<MainActivityAdapter.ViewHolder> {
    ArrayList<ChildMovie> movies = new ArrayList<>();
    private int rowlayout;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_Avatar) ImageView img;
        @BindView(R.id.tv_MovieName) TextView tv1;
        @BindView(R.id.tv_ReleaseDate) TextView tv2;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this,v);
        }

    }


    public MainActivityAdapter(ArrayList<ChildMovie> movies, int rowlayout, Context context){
        this.movies = movies;
        this.rowlayout = rowlayout;
        this.context = context;
    }


    @Override
    public MainActivityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowlayout,parent,false);
        ButterKnife.bind(this,view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv1.setText(movies.get(position).getTitle());
        holder.tv2.setText(movies.get(position).getRelease_date());
        Glide.with(context)
                .load("http://image.tmdb.org/t/p/w500"+movies.get(position).getPoster_path())
                .into(holder.img);


    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
