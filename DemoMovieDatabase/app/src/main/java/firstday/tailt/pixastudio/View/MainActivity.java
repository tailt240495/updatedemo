package firstday.tailt.pixastudio.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import firstday.tailt.pixastudio.Presenter.MainPresenter;
import firstday.tailt.pixastudio.demomoviedatabase.R;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.rv_Movie) RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
        MainPresenter presenter = new MainPresenter(this,recyclerView);
        presenter.getData();

    }
}
