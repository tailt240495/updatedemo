package firstday.tailt.pixastudio.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tai on 09/10/2016.
 */

public class Movie {

    @SerializedName("results")
    private ArrayList<ChildMovie> results;

    public ArrayList<ChildMovie> getResults() {
        return results;
    }
    public void setResults(ArrayList<ChildMovie> results) {
        this.results = results;
    }

}
