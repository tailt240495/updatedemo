package firstday.tailt.pixastudio.View;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.ButterKnife;
import firstday.tailt.pixastudio.demomoviedatabase.R;

public class InformationActivity extends AppCompatActivity {

    @BindView(R.id.iv_BackDrop) ImageView img_back;
    @BindView(R.id.tv_Language) TextView tvLanguage;
    @BindView(R.id.tv_VoteCount) TextView tvVoteCount;
    @BindView(R.id.tv_Average) TextView tvAverage;
    @BindView(R.id.tv_Overview) TextView tvOverview;
    @BindView(R.id.btn_Back) Button btn_Back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        ButterKnife.bind(this);
        Intent it = getIntent();
        Bundle b = it.getExtras();
        Glide.with(this)
                .load("http://image.tmdb.org/t/p/w500"+b.getString("image"))
                .into(img_back);
        tvLanguage.setText(b.getString("language"));
        tvVoteCount.setText(String.valueOf(b.getInt("voteCount")));
        tvAverage.setText(String.valueOf(b.getFloat("voteAvg")));
        tvOverview.setText(b.getString("overView"));
        btn_Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

}
