package firstday.tailt.pixastudio.Presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import java.util.ArrayList;


import butterknife.BindView;
import butterknife.ButterKnife;
import firstday.tailt.pixastudio.Adapter.MainActivityAdapter;
import firstday.tailt.pixastudio.Adapter.MainActivityClickListener;
import firstday.tailt.pixastudio.Model.APIMovie;
import firstday.tailt.pixastudio.Model.ChildMovie;
import firstday.tailt.pixastudio.Model.Movie;
import firstday.tailt.pixastudio.View.InformationActivity;
import firstday.tailt.pixastudio.demomoviedatabase.R;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;


public class MainPresenter {

    private Context context;

    RecyclerView recyclerView;

    String url = "http://api.themoviedb.org/3/";

    String apikey = "5446ca07c8dacacf459775023ac5c116";

    Bundle b = new Bundle();

    public MainPresenter(Context context, RecyclerView recyclerViewC) {
        this.context = context;
        recyclerView = recyclerViewC;
        recyclerView.setLayoutManager(new GridLayoutManager(context,2));
    }

    public void getData(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        APIMovie service = retrofit.create(APIMovie.class);

        service.getTopRatedMovies(apikey).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Response<Movie> response, Retrofit retrofit) {
                final ArrayList<ChildMovie> movies = response.body().getResults();
                recyclerView.setAdapter(new MainActivityAdapter(movies, R.layout.main_row, context));

                recyclerView.addOnItemTouchListener(new MainActivityClickListener(context, new MainActivityClickListener.OnItemClickListener(){
                    @Override public void onItemClick(View view, int position) {

                        b.putString("language",movies.get(position).getOriginal_language());
                        b.putString("image",movies.get(position).getBackdrop_path());
                        b.putInt("voteCount",movies.get(position).getVote_count());
                        b.putFloat("voteAvg",movies.get(position).getVote_average());
                        b.putString("overView",movies.get(position).getOverview());

                        Intent it = new Intent(context, InformationActivity.class);
                        it.putExtras(b);
                        context.startActivity(it);

                    }
                }));
            }

            @Override
            public void onFailure(Throwable t) {
            }
        });
    }

}
